FROM ubuntu:20.04
MAINTAINER ychuang@bigobject.io

RUN apt-get update \
	&& DEBIAN_FRONTEND=noninteractive  apt-get install -y --no-install-recommends \ 
	build-essential libpq-dev nodejs mysql-client netcat-openbsd locales
RUN apt-get install -y sudo
RUN sudo apt-get install -y python3-pip && sudo apt-get install -y build-essential libssl-dev libffi-dev python-dev && sudo apt-get install -y python3-venv && sudo apt-get install -y wget && sudo apt-get install -y dpkg && sudo apt-get install -y libodbc1 && sudo apt-get install -y odbcinst1debian2 && sudo apt-get install -y unixodbc-dev && sudo apt-get install -y libmariadb-java


RUN sudo apt-get install -y default-jre && sudo apt-get install -y default-jdk

RUN sudo wget https://repo.mysql.com/apt/ubuntu/pool/mysql-tools/m/mysql-connector-j/mysql-connector-j_8.0.31-1ubuntu20.04_all.deb
RUN sudo wget https://repo.mysql.com/apt/ubuntu/pool/mysql-8.0/m/mysql-community/mysql-community-client-plugins_8.0.27-1ubuntu18.04_amd64.deb
RUN sudo wget https://repo.mysql.com/apt/ubuntu/pool/mysql-tools/m/mysql-connector-odbc/mysql-connector-odbc_8.0.27-1ubuntu18.04_amd64.deb
RUN sudo dpkg -i mysql-community-client-plugins_8.0.27-1ubuntu18.04_amd64.deb && sudo dpkg -i mysql-connector-odbc_8.0.27-1ubuntu18.04_amd64.deb && sudo dpkg -i mysql-connector-j_8.0.31-1ubuntu20.04_all.deb

ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000
ARG DAUX_HOME=/daux.io

RUN groupadd -g ${gid} ${group} \
    && useradd -d "$DAUX_HOME" -u ${uid} -g ${gid} -m -s /bin/bash ${user}   

RUN sudo apt-get install -y ruby && sudo apt-get install -y libcurl4-openssl-dev && sudo apt-get install -y ruby-dev

RUN gem install curb && gem install minitest-reporters && gem install shoulda-context && gem install rest-client && gem install json

RUN sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=en_US.UTF-8

RUN pip3 install faker && pip3 install mysql-connector && pip3 install colorama && pip3 install pyodbc && pip3 install jaydebeapi && pip3 install pytest && pip3 install pytest-html

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
ENV LC_ALL en_US.UTF-8
ENV CLASSPATH=/usr/share/java/mysql.jar
RUN export CLASSPATH

# default working directory
WORKDIR /botest

VOLUME /botest

USER ${user}

CMD ["sh","botest_python.sh"]
