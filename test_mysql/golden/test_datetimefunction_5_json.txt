adddate(a,INTERVAL 30 second)
2020-09-29 09:58:50
adddate(a,INTERVAL 30 minute)
2020-09-29 10:28:20
adddate(a,INTERVAL 30 hour)
2020-09-30 15:58:20
adddate(a,INTERVAL 30 day)
2020-10-29 09:58:20
adddate(a,INTERVAL 30 week)
2021-04-27 09:58:20
adddate(a,INTERVAL 30 month)
2023-03-29 09:58:20
adddate(a,INTERVAL 30 quarter)
2028-03-29 09:58:20
adddate(a,30)
2020-10-29 09:58:20
adddate(a,'1d20h30m')
2020-10-01 06:28:20
addtime(a,INTERVAL 30 second)
2020-09-29 09:58:50
addtime(a,INTERVAL 30 minute)
2020-09-29 10:28:20
addtime(a,INTERVAL 30 hour)
2020-09-30 15:58:20
addtime(a,INTERVAL 30 day)
2020-10-29 09:58:20
addtime(a,INTERVAL 30 week)
2021-04-27 09:58:20
addtime(a,INTERVAL 30 month)
2023-03-29 09:58:20
addtime(a,INTERVAL 30 quarter)
2028-03-29 09:58:20
addtime(a,30)
2020-09-29 09:58:50
addtime(a,'1d20h30m')
2020-10-01 06:28:20
addtime(a,'10:10:10')
2020-09-29 20:08:30
subdate(a,INTERVAL 30 second)
2020-09-29 09:57:50
subdate(a,INTERVAL 30 minute)
2020-09-29 09:28:20
subdate(a,INTERVAL 30 hour)
2020-09-28 03:58:20
subdate(a,INTERVAL 30 day)
2020-08-30 09:58:20
subdate(a,INTERVAL 30 week)
2020-03-03 09:58:20
subdate(a,INTERVAL 30 month)
2018-03-29 09:58:20
subdate(a,INTERVAL 30 quarter)
2013-03-29 09:58:20
subdate(a,30)
2020-08-30 09:58:20
subdate(a,'1d20h30m')
2020-09-27 13:28:20
