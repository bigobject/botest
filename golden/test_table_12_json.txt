
stmt=insert into ctable values (1), (2), (3)

stmt=desc ctable
{"schema"=>{"attr"=>[{"name"=>"_cts", "type"=>"TIMESTAMP"}, {"name"=>"a", "type"=>"INT32"}]}, "size"=>3, "create_stmt"=>"CREATE circular column TABLE ctable ('a' INT32)"}
stmt=desc ctable
{"schema"=>{"attr"=>[{"name"=>"_cts", "type"=>"TIMESTAMP"}, {"name"=>"a", "type"=>"INT32"}]}, "size"=>0, "create_stmt"=>"CREATE circular column TABLE ctable ('a' INT32)"}
stmt=select count(*) from ctable
[[0]]