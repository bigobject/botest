
stmt=CREATE TREE testtree1 (v1 DOUBLE, SUM(Data)) FROM sales GROUP BY Product.brand, Customer.state

stmt=APPLY put(v1,10) TO testtree1 AT Product.brand ORDER BY Product.brand,v1 LIMIT 10
[["Aquarius", 10, 32.72], ["Cheetos", 10, 79], ["Minute Maid", 10, 33.04], ["Ocean Spray", 10, 77.12], ["Pepsi", 10, 37.7]]