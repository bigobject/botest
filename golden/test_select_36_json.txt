
stmt=register host='127.0.01' port = 9091 alias = 'abc'

stmt=select case when a=0 then 123 end from t
[[nil], [nil]]
stmt=select case when a=1 then '123' end from t
[["123"], [nil]]
stmt=select case when a='' then 123 end from t
[[nil], [nil]]
stmt=select case when b='abc' then '123' end from t
[["123"], [nil]]
stmt=select case when b='' then 123 end from t
[[nil], [123]]
stmt=select case when a=1 then '123' else '456' end from t
[["123"], ["456"]]
stmt=select case when b='abc' then 123 else null end from t
[[123], [nil]]
stmt=select case when a =1 then 'ABC' else a end as bb from a group by bb
[["ABC"], ["3"]]
stmt=select case when c = 'a' then 'QWE' else c end as bb from b group by bb
[["QWE"], ["d"]]
stmt=select case when a=0 then b else c end from c group by a
[["qwe"]]
stmt=select case when a=0 then b else c end from c1 group by a
[["qwe"]]
stmt=select case when a=0 then b else c end from c2 group by a
[["qwe"]]
stmt=cluster select a,b,c,d, case when e = 5 then f when e <> 0 then g end as change1, h,i,j,(h * i * j) as change2 from t1 group by a
[[1, 2, 3, 4, 6, 8, 9, 10, 720]]
stmt=cluster select a,b,c,d, case when e = 5 then f when e <> 0 then g end , h,i,j,(h * i * j) from t1 group by a
[[1, 2, 3, 4, 6, 8, 9, 10, 720]]