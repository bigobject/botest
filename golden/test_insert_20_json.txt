
stmt=use TEST

stmt=insert into TEST.t_int values('text')
Incorrect data value: text
stmt=insert into TEST.t_int(a,b) values('null',0)
Syntax error: 'null' is not recognized
stmt=insert into TEST.t_int values(12345678901)
Out of range data: 12345678901
stmt=insert into TEST.t_int values(NULL)
Invalid value: NULL
stmt=insert into TEST.t_int values(null)
Invalid value: null
stmt=insert into TEST.t_smallint values('text')
Incorrect data value: text
stmt=insert into TEST.t_smallint(a,b) values('null',0)
Syntax error: 'null' is not recognized
stmt=insert into TEST.t_smallint values(123456)
Out of range data: 123456
stmt=insert into TEST.t_smallint values(NULL)
Invalid value: NULL
stmt=insert into TEST.t_smallint values(null)
Invalid value: null
stmt=insert into TEST.t_bigint values('text')
Incorrect data value: text
stmt=insert into TEST.t_bigint(a,b) values('null',0)
Syntax error: 'null' is not recognized
stmt=insert into TEST.t_bigint values(12345678901234567890)
Out of range data: 12345678901234567890
stmt=insert into TEST.t_bigint values(NULL)
Invalid value: NULL
stmt=insert into TEST.t_bigint values(null)
Invalid value: null
stmt=insert into TEST.t_string values('12345678912345678912345678912345678912345678912345678912345678912')
Invalid value: 12345678912345678912345678912345678912345678912345678912345678912
stmt=insert into TEST.t_string values(null)
Invalid value: null
stmt=insert into TEST.t_string values(Null)
Invalid value: Null
stmt=insert into TEST.t_char values(null)
Invalid value: null
stmt=insert into TEST.t_char values(Null)
Invalid value: Null
stmt=insert into TEST.t_char values('123456789012345678901234567890123')
Invalid value: 123456789012345678901234567890123
stmt=insert into TEST.t_varchar(a) values(null)
Invalid value: null
stmt=insert into TEST.t_varchar values(Null)
Invalid value: Null
stmt=insert into TEST.t_char values('12345678901234567890123456789012345678901234567890123')
Invalid value: 12345678901234567890123456789012345678901234567890123
stmt=insert into TEST.t_float values(Null)
Invalid value: Null
stmt=insert into TEST.t_float values(null)
Invalid value: null
stmt=insert into TEST.t_float values('y2023.3')
Incorrect data value: y2023.3
stmt=insert into TEST.t_double values(Null)
Invalid value: Null
stmt=insert into TEST.t_double values(null)
Invalid value: null
stmt=insert into TEST.t_double values('y2023.3')
Incorrect data value: y2023.3
stmt=insert into TEST.t_date values('2023-13-01')
Incorrect date value: 2023-13-01
stmt=insert into TEST.t_date values('2023-01-32')
Incorrect date value: 2023-01-32
stmt=insert into TEST.t_date values('2023-02-30')
Incorrect date value: 2023-02-30
stmt=insert into TEST.t_date values('Null')
Incorrect date value: Null
stmt=insert into TEST.t_date values('null')
Incorrect date value: null
stmt=insert into TEST.t_datetime values('2023-13-01 00:00:00')
Incorrect date value: 2023-13-01 00:00:00
stmt=insert into TEST.t_datetime values('2023-01-32 00:00:00')
Incorrect date value: 2023-01-32 00:00:00
stmt=insert into TEST.t_datetime values('2023-02-31 00:00:00')

stmt=insert into TEST.t_datetime values('2023-01-01 25:00:00')
Incorrect date value: 2023-01-01 25:00:00
stmt=insert into TEST.t_datetime values('2023-01-01 00:61:00')
Incorrect date value: 2023-01-01 00:61:00
stmt=insert into TEST.t_datetime values('Null')
Incorrect date value: Null
stmt=insert into TEST.t_datetime values('null')
Incorrect date value: null
stmt=insert into TEST.t_ipv4 values('256.0.0.0')
Invalid value: 256.0.0.0
stmt=insert into TEST.t_ipv4 values(Null)
Invalid value: Null
stmt=insert into TEST.t_ipv4 values(null)
Invalid value: null
stmt=insert into TEST.t_ipv6(a) values('2001:4f8:3:ba:2e0:81ff:fe22:d1f11899')
Syntax error: '2001:4f8:3:ba:2e0:81ff:fe22...' is not recognized
stmt=insert into TEST.t_ipv6 values(Null)
Invalid value: Null
stmt=insert into TEST.t_ipv6 values(null)
Invalid value: null