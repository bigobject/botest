
stmt=CREATE TREE testtree1 (v1 DOUBLE, SUM(Data), v2 INT32) FROM sales GROUP BY Product.brand, Customer.state

stmt=CREATE ALL TREE testtree3 (v1 DOUBLE, SUM(Data), v2 INT32) FROM sales GROUP BY Product.brand, Customer.state

stmt=APPLY pareto(40,20) TO testtree1 AT Product.brand
[]
stmt=APPLY put(v2, 5) TO testtree1 AT '/Ocean Spray'
[["Ocean Spray", 0, 77.12, 5]]
stmt=APPLY put(v1, 0.1) TO testtree1 AT '/Ocean Spray'
[["Ocean Spray", 0.1, 77.12, 5]]
stmt=APPLY pareto(40,50) TO testtree3 AT Product.brand ORDER BY Product.brand
[["Aquarius", 0.500917, 32.72, 0], ["Cheetos", 0.680633, 79, 0], ["Minute Maid", 0.432506, 33.04, 0], ["Ocean Spray", 0.428423, 77.12, 0], ["Pepsi", 0.569231, 37.7, 0]]
stmt=GET * FROM testtree1 by '/Pepsi/*' order by SUM(Data)
[["Pepsi", "California", 0, 16.24, 0], ["Pepsi", "Hawaii", 0, 21.46, 0]]
stmt=GET 'Product.brand', v1, v2 , SUM(Data) FROM testtree1 by '/*' order by SUM(Data)
[["Aquarius", 0, 0, 32.72], ["Minute Maid", 0, 0, 33.04], ["Pepsi", 0, 0, 37.7], ["Ocean Spray", 0.1, 5, 77.12], ["Cheetos", 0, 0, 79]]
stmt=GET * FROM testtree3 by '/' order by SUM(Data)
[["/", 0, 259.58, 0]]
stmt=GET * FROM testtree3 by '/*' order by SUM(Data)
[["Aquarius", 0.500917, 32.72, 0], ["Minute Maid", 0.432506, 33.04, 0], ["Pepsi", 0.569231, 37.7, 0], ["Ocean Spray", 0.428423, 77.12, 0], ["Cheetos", 0.680633, 79, 0]]
stmt=GET * FROM testtree3 by '/*/*' order by SUM(Data)
[["Aquarius", "Ohio", 0, 2.57, 0], ["Minute Maid", "Hawaii", 0, 8.36, 0], ["Cheetos", "Ohio", 0, 10.15, 0], ["Minute Maid", "Kentucky", 0, 10.39, 0], ["Aquarius", "Hawaii", 0, 13.76, 0], ["Minute Maid", "California", 0, 14.29, 0], ["Cheetos", "Hawaii", 0, 15.08, 0], ["Pepsi", "California", 0, 16.24, 0], ["Aquarius", "Kentucky", 0, 16.39, 0], ["Ocean Spray", "Kentucky", 0, 18.83, 0], ["Pepsi", "Hawaii", 0, 21.46, 0], ["Cheetos", "Kentucky", 0, 22.78, 0], ["Ocean Spray", "Hawaii", 0, 25.25, 0], ["Cheetos", "California", 0, 30.99, 0], ["Ocean Spray", "Ohio", 0, 33.04, 0]]
stmt=GET * FROM testtree3 by '/*/*/*' order by SUM(Data)
[["Ocean Spray", "Ohio", 17, 0, 0.21, 0], ["Minute Maid", "Kentucky", 24, 0, 0.83, 0], ["Aquarius", "Ohio", 13, 0, 0.95, 0], ["Minute Maid", "California", 26, 0, 1.52, 0], ["Aquarius", "Kentucky", 12, 0, 1.61, 0], ["Aquarius", "Kentucky", 7, 0, 1.61, 0], ["Aquarius", "Ohio", 11, 0, 1.62, 0], ["Cheetos", "Ohio", 6, 0, 3.75, 0], ["Cheetos", "Kentucky", 29, 0, 4.09, 0], ["Ocean Spray", "Hawaii", 22, 0, 4.82, 0], ["Cheetos", "Hawaii", 5, 0, 5.66, 0], ["Cheetos", "Ohio", 3, 0, 6.4, 0], ["Pepsi", "California", 20, 0, 6.64, 0], ["Minute Maid", "Hawaii", 19, 0, 8.36, 0], ["Ocean Spray", "Hawaii", 23, 0, 8.87, 0], ["Pepsi", "Hawaii", 8, 0, 9, 0], ["Cheetos", "Hawaii", 27, 0, 9.42, 0], ["Minute Maid", "Kentucky", 18, 0, 9.56, 0], ["Pepsi", "California", 1, 0, 9.6, 0], ["Ocean Spray", "Hawaii", 10, 0, 11.56, 0], ["Pepsi", "Hawaii", 14, 0, 12.46, 0], ["Cheetos", "California", 25, 0, 12.53, 0], ["Minute Maid", "California", 16, 0, 12.77, 0], ["Ocean Spray", "Ohio", 9, 0, 12.85, 0], ["Aquarius", "Kentucky", 15, 0, 13.17, 0], ["Aquarius", "Hawaii", 2, 0, 13.76, 0], ["Cheetos", "California", 4, 0, 18.46, 0], ["Cheetos", "Kentucky", 21, 0, 18.69, 0], ["Ocean Spray", "Kentucky", 28, 0, 18.83, 0], ["Ocean Spray", "Ohio", 30, 0, 19.98, 0]]