
stmt=select * from (select a as 'a',a as 'a' from a)
Duplicate object: alias 'a'
stmt=select * from (select a, a from a)
Duplicate object: alias 'a'
stmt=remote select a as 'a',a as 'a' from a
Duplicate object: alias 'a'
stmt=remote select a,a from a
Duplicate object: alias 'a'
stmt=cluster select a as 'a',a as 'a' from a
Duplicate object: alias 'a'
stmt=cluster select a,a from a
Duplicate object: alias 'a'
stmt=create table b as (select 'abc' as 'a', 'qwe' as 'a')
Duplicate object: alias 'a'
stmt=create table b as (select 'abc', 'qwe')

stmt=remote select * from (select 1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7)
Alias needed: column '1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7' too long.
stmt=remote select * from (select 1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7+1+2+3+4+5+6+7 as 'a')
[[168]]
stmt=remote select * from (select 'asdfdgregrefewfhejfheufhujdiwqdjiqwewdewdeiofjoeifjeiowjfioewjewfewiofjewijfq')
Alias needed: column 'asdfdgregrefewfhejfheufhujdiwqdjiqwewdewdeiofjoeifjeiowjfioewjewfewiofjewijfq' too long.
stmt=remote select * from (select 'asdfdgregrefewfhejfheufhujdiwqdjiqwewdewdeiofjoeifjeiowjfioewjewfewiofjewijfq' as 'a')
[["asdfdgregrefewfhejfheufhujdiwqdjiqwewdewdeiofjoeifjeiowjfioewjewfewiofjewijfq"]]
stmt=SELECT (NOT ISNULL(DATE_FORMAT(SUBSTRING(`d`.`a`, 1, 1024), '%Y'))) from d
[[1]]
stmt=select * from (SELECT (NOT ISNULL(DATE_FORMAT(SUBSTRING(`d`.`a`, 1, 1024), '%Y'))) from d group by 1)
[[1]]
stmt=remote select * from (SELECT (NOT ISNULL(DATE_FORMAT(SUBSTRING(`d`.`a`, 1, 1024), '%Y')))from d)
[[1]]
stmt=remote select * from (SELECT (NOT ISNULL(DATE_FORMAT(SUBSTRING(`d`.`a`, 1, 1024), '%Y'))) as 'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww' from d)
Invalid name: alias 'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww' too long.
stmt=remote select * from (SELECT (NOT ISNULL(DATE_FORMAT(SUBSTRING(`d`.`a`, 1, 1024), '%Y'))) as 'a' from d)
[[1]]
stmt=create table c as select * from(remote select * from (SELECT (NOT ISNULL(DATE_FORMAT(SUBSTRING(`d`.`a`, 1, 1024), '%Y')))from d))
