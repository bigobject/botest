
stmt=select adddate(a, interval 30 SECOND) from a
[["2020-09-29 09:58:50"]]
stmt=select adddate(a, interval 30 MINUTE) from a
[["2020-09-29 10:28:20"]]
stmt=select adddate(a, interval 30 HOUR) from a
[["2020-09-30 15:58:20"]]
stmt=select adddate(a, interval 30 DAY) from a
[["2020-10-29 09:58:20"]]
stmt=select adddate(a, interval 30 WEEK) from a
[["2021-04-27 09:58:20"]]
stmt=select adddate(a, interval 30 MONTH) from a
[["2023-03-29 09:58:20"]]
stmt=select adddate(a, interval 30 QUARTER) from a
[["2028-03-29 09:58:20"]]
stmt=select adddate(a, 30) from a
[["2020-10-29 09:58:20"]]
stmt=select adddate(a, '1d20h30m') from a
[["2020-10-01 06:28:20"]]
stmt=select addtime(a, interval 30 SECOND) from a
[["2020-09-29 09:58:50"]]
stmt=select addtime(a, interval 30 MINUTE) from a
[["2020-09-29 10:28:20"]]
stmt=select addtime(a, interval 30 HOUR) from a
[["2020-09-30 15:58:20"]]
stmt=select addtime(a, interval 30 DAY) from a
[["2020-10-29 09:58:20"]]
stmt=select addtime(a, interval 30 WEEK) from a
[["2021-04-27 09:58:20"]]
stmt=select addtime(a, interval 30 MONTH) from a
[["2023-03-29 09:58:20"]]
stmt=select addtime(a, interval 30 QUARTER) from a
[["2028-03-29 09:58:20"]]
stmt=select addtime(a, 30) from a
[["2020-09-29 09:58:50"]]
stmt=select addtime(a, '1d20h30m') from a
[["2020-10-01 06:28:20"]]
stmt=select addtime(a, '10:10:10') from a
[["2020-09-29 20:08:30"]]
stmt=select subdate(a, interval 30 SECOND) from a
[["2020-09-29 09:57:50"]]
stmt=select subdate(a, interval 30 MINUTE) from a
[["2020-09-29 09:28:20"]]
stmt=select subdate(a, interval 30 HOUR) from a
[["2020-09-28 03:58:20"]]
stmt=select subdate(a, interval 30 DAY) from a
[["2020-08-30 09:58:20"]]
stmt=select subdate(a, interval 30 WEEK) from a
[["2020-03-03 09:58:20"]]
stmt=select subdate(a, interval 30 MONTH) from a
[["2018-03-29 09:58:20"]]
stmt=select subdate(a, interval 30 QUARTER) from a
[["2013-03-29 09:58:20"]]
stmt=select subdate(a, 30) from a
[["2020-08-30 09:58:20"]]
stmt=select subdate(a, '1d20h30m') from a
[["2020-09-27 13:28:20"]]