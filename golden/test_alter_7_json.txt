
stmt=Alter table test add column col1 STRING not null default 'a'

stmt=select * from test where col1 <> ''
[]
stmt=select * from test where col1 = ''
[[1, ""]]
stmt=Alter table test add column col2 CHAR not null default 'a'

stmt=Alter table test add column col3 INT4 not null default 2123123

stmt=Alter table test add column col4 smallint not null default 5646

stmt=Alter table test add column col5 bigint not null default 1231241

stmt=Alter table test add column col6 biginteger not null default 45654564

stmt=Alter table test add column col7 FLOAT4 not null default 20.5465465

stmt=Alter table test add column col8 DOUBLE not null default 45.55646454654

stmt=Alter table test add column col9 DATE not null default '2023-01-08'

stmt=Alter table test add column col10 DATETIME not null default '2023-01-08 23:22:12'

stmt=Alter table test add column col11 VARSTRING not null default 'jack'

stmt=insert into test values('aa', 'a string', 2, 8, 156, 32, 1.14, 2.26464, '2016-01-01', '2016-01-01 02:03:04','jennifer')
Incorrect data value: aa
stmt=desc table test
{"schema"=>{"attr"=>[{"name"=>"a", "type"=>"INT4"}, {"name"=>"col1", "type"=>"STRING(63)"}, {"name"=>"col2", "type"=>"CHAR(32)"}, {"name"=>"col3", "type"=>"INT4"}, {"name"=>"col4", "type"=>"INT16"}, {"name"=>"col5", "type"=>"BIGINTEGER"}, {"name"=>"col6", "type"=>"BIGINTEGER"}, {"name"=>"col7", "type"=>"FLOAT4"}, {"name"=>"col8", "type"=>"DOUBLE"}, {"name"=>"col9", "type"=>"DATE32"}, {"name"=>"col10", "type"=>"DATETIME64"}, {"name"=>"col11", "type"=>"VARSTRING(255)"}]}, "size"=>1, "create_stmt"=>"CREATE column TABLE test ('a' INT4, 'col1' STRING(63), 'col2' CHAR(32), 'col3' INT4, 'col4' INT16, 'col5' BIGINTEGER, 'col6' BIGINTEGER, 'col7' FLOAT4, 'col8' DOUBLE, 'col9' DATE32, 'col10' DATETIME64, 'col11' VARSTRING(255))"}
stmt=select * from test
[[1, "", "", 0, 0, 0, 0, 0, 0, "0-00-00", "0-00-00 00:00:00", ""]]