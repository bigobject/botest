
stmt=SELECT pageURL, pageRank FROM rankings WHERE pageRank > 1000
[["nbizrgdziebsaecsecujfjcqtvnpcnxxwiopmddorcxnlijdizgoi", 80818], ["oiavraahcejwugzssvrzakqseqkbfwvmhhqjdmxzjlqimypmsglukddrsaalbnnt", 11289], ["xkepmjgbjzogibrqtyodqcxtjrqwupifrtwoocradcbmrtilmzemgstjqiuaqzqegpvqmsvjndhcnzsoqszihncwberdy", 8097]]
stmt=SELECT pageURL, pageRank FROM rankings WHERE pageRank > 11289
[["nbizrgdziebsaecsecujfjcqtvnpcnxxwiopmddorcxnlijdizgoi", 80818]]
stmt=SELECT sourceIP, SUM(adRevenue) FROM uservisits GROUP BY sourceIP
[["182.163.112.4", 0.522919], ["146.97.78.95", 1.884784], ["227.209.164.46", 0.636746], ["35.143.225.164", 1.53174], ["34.57.45.175", 0.432755], ["46.160.116.124", 0.50766], ["34.215.106.253", 0.015384], ["227.220.150.185", 0.789191], ["141.8.175.108", 0.699592], ["170.180.22.25", 0.00876], ["189.231.126.117", 0.427352], ["195.24.155.161", 0.123671], ["230.117.9.160", 0.284009], ["10.76.51.112", 0.603984], ["60.64.81.137", 0.825752]]
stmt=SELECT sourceIP, SUM(adRevenue) ,AVG(rankings.pageRank) FROM uservisits WHERE visitDate > '1981-08-01' and visitDate < '1981-12-01' GROUP BY sourceIP ORDER BY SUM(adRevenue) DESC
[["10.76.51.112", 0.603984, 8097], ["182.163.112.4", 0.190202, 11289]]
stmt=SELECT sourceIP, SUM(adRevenue) ,AVG(rankings.pageRank) FROM uservisits WHERE visitDate > '1990-01-01' and visitDate < '2000-01-01' GROUP BY sourceIP ORDER BY SUM(adRevenue) DESC
[["146.97.78.95", 1.884784, 46053.5], ["35.143.225.164", 0.879263, 80818], ["227.220.150.185", 0.789191, 11289], ["230.117.9.160", 0.284009, 8097], ["227.209.164.46", 0.115967, 80818], ["34.215.106.253", 0.015384, 11289], ["170.180.22.25", 0.00876, 11289]]
stmt=SELECT sourceIP, SUM(adRevenue) ,AVG(rankings.pageRank) FROM uservisits WHERE visitDate > '1981-01-01' and visitDate < '2012-01-01' GROUP BY sourceIP ORDER BY SUM(adRevenue) DESC
[["146.97.78.95", 1.884784, 46053.5], ["35.143.225.164", 0.879263, 80818], ["227.220.150.185", 0.789191, 11289], ["227.209.164.46", 0.636746, 46053.5], ["10.76.51.112", 0.603984, 8097], ["46.160.116.124", 0.50766, 11289], ["34.57.45.175", 0.432755, 46053.5], ["189.231.126.117", 0.427352, 8097], ["230.117.9.160", 0.284009, 8097], ["182.163.112.4", 0.190202, 11289], ["195.24.155.161", 0.123671, 8097], ["34.215.106.253", 0.015384, 11289], ["170.180.22.25", 0.00876, 11289]]
stmt=CREATE COLUMN TABLE uservisits_sorted AS (SELECT * from uservisits_ts ORDER BY visitDate)

stmt=SELECT sourceIP, SUM(adRevenue) ,AVG(rankings.pageRank) FROM uservisits_sorted BETWEEN '1981-08-01' AND '1981-12-01' of visitDate GROUP BY sourceIP ORDER BY SUM(adRevenue) DESC
[["10.76.51.112", 0.603984, 8097], ["182.163.112.4", 0.190202, 11289]]
stmt=SELECT sourceIP, SUM(adRevenue) ,AVG(rankings.pageRank) FROM uservisits_sorted BETWEEN '1990-01-01' AND '2000-01-01' of visitDate GROUP BY sourceIP ORDER BY SUM(adRevenue) DESC
[["146.97.78.95", 1.884784, 46053.5], ["35.143.225.164", 0.879263, 80818], ["227.220.150.185", 0.789191, 11289], ["230.117.9.160", 0.284009, 8097], ["227.209.164.46", 0.115967, 80818], ["34.215.106.253", 0.015384, 11289], ["170.180.22.25", 0.00876, 11289]]
stmt=SELECT sourceIP, SUM(adRevenue) ,AVG(rankings.pageRank) FROM uservisits_sorted BETWEEN '1981-01-01' AND '2012-01-01' of visitDate GROUP BY sourceIP ORDER BY SUM(adRevenue) DESC
[["146.97.78.95", 1.884784, 46053.5], ["35.143.225.164", 0.879263, 80818], ["227.220.150.185", 0.789191, 11289], ["227.209.164.46", 0.636746, 46053.5], ["10.76.51.112", 0.603984, 8097], ["46.160.116.124", 0.50766, 11289], ["34.57.45.175", 0.432755, 46053.5], ["189.231.126.117", 0.427352, 8097], ["230.117.9.160", 0.284009, 8097], ["182.163.112.4", 0.190202, 11289], ["195.24.155.161", 0.123671, 8097], ["34.215.106.253", 0.015384, 11289], ["170.180.22.25", 0.00876, 11289]]