
stmt=select * from test
[[1, "abc"], [2, "qwe"], [3, "rty"], [2, "abcdefgh"]]
stmt=select substr(name,1,0) from test
[[""], [""], [""], [""]]
stmt=select substr(name,-1) from test
[["abc"], ["qwe"], ["rty"], ["abcdefgh"]]
stmt=select substr(name,2) from test
[["bc"], ["we"], ["ty"], ["bcdefgh"]]
stmt=select substr(name,'1','2') from test
[["ab"], ["qw"], ["rt"], ["ab"]]
stmt=select substr(name,'v','a') from test
[[""], [""], [""], [""]]
stmt=select id, substr(name,40000000000000000000000000000000) as n from test group by id
[[1, ""], [2, ""], [3, ""]]
stmt=select id, substr(name,-40000000000000000000000000000000,40000000000000000000000000) as n from test group by id
[[1, "abc"], [2, "qwe"], [3, "rty"]]
stmt=select id, substr(name,40000000000000000000000000000000,-4000000000000000000000000000) as n from test group by id
[[1, ""], [2, ""], [3, ""]]
stmt=select id, substr(name,2,2) as n from test
[[1, "bc"], [2, "we"], [3, "ty"], [2, "bc"]]
stmt=select id, substr(name,2,2) as n from test group by id
[[1, "bc"], [2, "we"], [3, "ty"]]