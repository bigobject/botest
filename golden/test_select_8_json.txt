
stmt=select MAX(age8), MIN(age8), SUM(age8), COUNT(age8), AVG(age8), STD(age8), STDDEV_SAMP(age8), NORM(age8) from ages
[[20, 5, 35, 3, 11.666667, 6.236096, 7.637626, 1]]
stmt=select MAX(age16), MIN(age16), SUM(age16), COUNT(age16), AVG(age16), STD(age16), STDDEV_SAMP(age16), NORM(age16) from ages
[[20, 5, 35, 3, 11.666667, 6.236096, 7.637626, 1]]
stmt=select MAX(age32), MIN(age32), SUM(age32), COUNT(age32), AVG(age32), STD(age32), STDDEV_SAMP(age32), NORM(age32) from ages
[[20, 5, 35, 3, 11.666667, 6.236096, 7.637626, 1]]
stmt=select MAX(age64), MIN(age64), SUM(age64), COUNT(age64), AVG(age64), STD(age64), STDDEV_SAMP(age64), NORM(age64) from ages
[[20, 5, 35, 3, 11.666667, 6.236096, 7.637626, 1]]
stmt=select MAX(ageF), MIN(ageF), SUM(ageF), COUNT(ageF), AVG(ageF), STD(ageF), STDDEV_SAMP(ageF), NORM(ageF) from ages
[[20, 5, 35, 3, 11.666667, 6.236096, 7.637626, 1]]
stmt=select MAX(ageD), MIN(ageD), SUM(ageD), COUNT(ageD), AVG(ageD), STD(ageD), STDDEV_SAMP(ageD), NORM(ageD) from ages
[[20, 5, 35, 3, 11.666667, 6.236096, 7.637626, 1]]