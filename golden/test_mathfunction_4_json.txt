
stmt=select (4/2)%2
[[0]]
stmt=select ((1+3) / 4 * 5)
[[5]]
stmt=select (a*b) %a from b
[[0], [0]]
stmt=select ((a+b)/b*c) from b
[[8], [80]]
stmt=select (((a-b) -a)*(c+b)/a*(b+c)) from b
[[-208], [-21060]]
stmt=select 540<<1 
[[1080]]
stmt=select 540>>2
[[135]]
stmt=select 500&2
[[0]]
stmt=select 500|2
[[502]]
stmt=select * from b where  a>b
[]
stmt=select * from b where  a<b
[[4, 5, 8], [40, 50, 80]]
stmt=select * from b where  a=b
[]
stmt=select * from b where  a<=1
[]
stmt=select * from b where  a>=b
[]
stmt=select * from b where  a<>b
[[4, 5, 8], [40, 50, 80]]
stmt=select * from b where  a!=b
[[4, 5, 8], [40, 50, 80]]
stmt=select * from test where   not a like "%e%"
[["jack"], ["john"]]
stmt=select * from test where   a like  "%o%"
[["joe"], ["joey"], ["john"]]
stmt=select * from test where 5 in (1,2,5) 
[["jack"], ["jennifer"], ["joe"], ["joey"], ["john"]]
stmt=select * from b where a>b and a=b
[]
stmt=select * from b where a>b or a=b
[]
stmt=select a%a from c 
[[0], [0]]
stmt=select a&a from c
[[10], [123214]]
stmt=select c*c from c
[[1450131.656682], [14646838347.91913]]
stmt=select a*b from c
[[241.241211], [134798721024]]
stmt=select b*b from c
[[581.973206], [1196882264064]]
stmt=select a*c from c
[[12042.141241], [14911866291.322]]
stmt=select a*a from c
[[100], [15181689796]]
stmt=select b+b from c
[[48.248241], [2188042.25]]
stmt=select a+a from c
[[20], [246428]]
stmt=select c-b from c
[[1180.090003], [-972997.002]]
stmt=select (a+10)-a from c
[[10], [10]]
stmt=select (b+12312.123)-c from c
[[11132.032946], [985309.127]]
stmt=select -a from c
[[-10], [-123214]]
stmt=select -b from c
[[-24.124121], [-1094021.125]]
stmt=select (b-10.2132)/b from c
[[0.57664], [0.999991]]
stmt=select (a+5)/a from c
[[1], [1]]
stmt=select * from d where date > '2022-12-01 12:00:00'
[]
stmt=select * from d where date < '2022-12-01 12:00:00'
[]
stmt=select '1998-01-08'>'1998-03-22'
[[0]]
stmt=select '1998-01-08'<'1998-03-22'
[[1]]
stmt=select '1998-01-08'>='1998-03-22'
[[0]]
stmt=select '1998-01-08'<='1998-03-22'
[[1]]
stmt=select '1998-01-08'='1998-03-22'
[[0]]
stmt=select '1998-03-22'='1998-03-22'
[[1]]
stmt=select 4>2
[[1]]
stmt=select 4>=2
[[1]]
stmt=select 2=2
[[1]]
stmt=select 3123.123>2
[[1]]
stmt=select 3123.123>=2
[[1]]
stmt=select 3123.123<=2
[[0]]
stmt=select 3123.123=3123.123
[[1]]
stmt=select 4<=2
[[0]]
stmt=select 2132.21312<21
[[0]]
stmt=select 'asdsad'>'asds'
[[1]]
stmt=select 'asdsad'<'asds'
[[0]]
stmt=select 'asdsad'<='asds'
[[0]]
stmt=select 'asdsad'>='asds'
[[1]]
stmt=select 'asdsad'='asds'
[[0]]
stmt=select 'asds'='asds'
[[1]]
stmt=select timestamp('2022-01-08')<=timestamp('2022-03-22')
[[1]]
stmt=select timestamp('2022-01-08')>=timestamp('2022-03-22')
[[0]]
stmt=select timestamp('2022-01-08')=timestamp('2022-01-08')
[[1]]
stmt=select 500>>2
[[125]]
stmt=select 500<<2
[[2000]]
stmt=select '2022-01-22' in ('2022-01-22','2022-01-08')
[[1]]
stmt=select 5 in (1,2,5)
[[1]]
stmt=select 1232.21412 in (1232.21412,1231241.421412,0.124124124)
[[1]]
stmt=select 'j' in ('jack','j','jennifer')
[[1]]
stmt=select 'jack' like 'jack'
[[1]]
stmt=select 'jack' like 'jennifer'
[[0]]
stmt=select 5 ^ 1
[[4]]
stmt=select 5|4
[[5]]