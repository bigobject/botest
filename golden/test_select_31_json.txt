
stmt=select * from t
[[0, 0, 0, 0, 0, 0], [nil, nil, nil, nil, nil, nil], [2, 4, 6, 8, 2.4, 4.8], [nil, nil, nil, 2, 2.4, 4.8], [2, 4, 8, nil, nil, nil], [2, nil, 4, nil, 6, nil], [nil, nil, nil, nil, nil, nil]]
stmt=select * from t where a <> null
[[0, 0, 0, 0, 0, 0], [2, 4, 6, 8, 2.4, 4.8], [2, 4, 8, nil, nil, nil], [2, nil, 4, nil, 6, nil]]
stmt=select * from t where a = null
[]
stmt=select * from t where b <> null
[[0, 0, 0, 0, 0, 0], [2, 4, 6, 8, 2.4, 4.8], [2, 4, 8, nil, nil, nil]]
stmt=select * from t where b = null
[]
stmt=select * from t where c <> null
[[0, 0, 0, 0, 0, 0], [2, 4, 6, 8, 2.4, 4.8], [2, 4, 8, nil, nil, nil], [2, nil, 4, nil, 6, nil]]
stmt=select * from t where c = null
[]
stmt=select * from t where d <> null
[[0, 0, 0, 0, 0, 0], [2, 4, 6, 8, 2.4, 4.8], [nil, nil, nil, 2, 2.4, 4.8]]
stmt=select * from t where d = null
[]
stmt=select * from t where e <> null
[[0, 0, 0, 0, 0, 0], [nil, nil, nil, nil, nil, nil], [2, 4, 6, 8, 2.4, 4.8], [nil, nil, nil, 2, 2.4, 4.8], [2, 4, 8, nil, nil, nil], [2, nil, 4, nil, 6, nil], [nil, nil, nil, nil, nil, nil]]
stmt=select * from t where e = null
[]
stmt=select * from t where f <> null
[[0, 0, 0, 0, 0, 0], [nil, nil, nil, nil, nil, nil], [2, 4, 6, 8, 2.4, 4.8], [nil, nil, nil, 2, 2.4, 4.8], [2, 4, 8, nil, nil, nil], [2, nil, 4, nil, 6, nil], [nil, nil, nil, nil, nil, nil]]
stmt=select * from t where f = null
[]
stmt=drop table t

stmt=set zero_if_null true

stmt=create table t(a int8, b int16, c int32, d int64, e float, f double)

stmt=insert into t values (0,0,0,0,0,0),(null,null,null,null,null,null),(2,4,6,8,2.4,4.8),()

stmt=select * from t
[[0, 0, 0, 0, 0, 0], [nil, nil, nil, nil, nil, nil], [2, 4, 6, 8, 2.4, 4.8], [nil, nil, nil, nil, nil, nil]]
stmt=select * from t where f <> null
[[0, 0, 0, 0, 0, 0], [nil, nil, nil, nil, nil, nil], [2, 4, 6, 8, 2.4, 4.8], [nil, nil, nil, nil, nil, nil]]
stmt=select * from t where f = null
[]
stmt=select * from t where f = 0
[[0, 0, 0, 0, 0, 0]]