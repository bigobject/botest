
stmt=select ceil(a) from a group by b
[[2]]
stmt=select ceiling(a) from a group by b
[[2]]
stmt=select cos(a) from a group by b
[[0.334238]]
stmt=select floor(a) from a group by b
[[1]]
stmt=select pi(a) from a group by b
[[3.141593]]
stmt=select pow(a,2) from a group by b
[[1.5129]]
stmt=select power(a,30) from a group by b
[[497.913092]]
stmt=select round(a) from a group by b
[[1]]
stmt=select sin(a) from a group by b
[[0.942489]]
stmt=select sqrt(a) from a group by b
[[1.109054]]
stmt=select truncate(1.1234,2)
[[1.12]]