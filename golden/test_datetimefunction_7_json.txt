
stmt=select DATEDIFF(a,b) from a
[[-458]]
stmt=select DATEDIFF(month,a,b) from a
[[15]]
stmt=select TIMESTAMPDIFF(a,b) from a
[[-458]]
stmt=select TIMESTAMPDIFF(year,a,b) from a
[[1]]
stmt=select TIMESTAMPDIFF(quarter,a,b) from a
[[5]]
stmt=select TIMESTAMPDIFF(month,a,b) from a
[[15]]
stmt=select TIMESTAMPDIFF(week,a,b) from a
[[65]]
stmt=select TIMESTAMPDIFF(day,a,b) from a
[[458]]
stmt=select TIMESTAMPDIFF(hour,a,b) from a
[[10994]]
stmt=select TIMESTAMPDIFF(minute,a,b) from a
[[659643]]
stmt=select TIMESTAMPDIFF(second,a,b) from a
[[39578585]]
stmt=select TIMEDIFF(a,b) from a
[["-2:03:05"]]
stmt=select DATE_TRUNC(a,'year') from a
[["2020-01-01 00:00:00"]]
stmt=select DATE_TRUNC(a,'quarter') from a
[["2020-07-01 00:00:00"]]
stmt=select DATE_TRUNC(a,'month') from a
[["2020-09-01 00:00:00"]]
stmt=select DATE_TRUNC(a,'week') from a
[["2020-09-28 00:00:00"]]
stmt=select DATE_TRUNC(a,'day') from a
[["2020-09-29 00:00:00"]]
stmt=select DATE_TRUNC(a,'hour') from a
[["2020-09-29 10:00:00"]]
stmt=select DATE_TRUNC(a,'minute') from a
[["2020-09-29 10:20:00"]]