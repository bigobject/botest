
stmt=create  table stable (a smallint not null default 123)TIMEBOUND('5s')ENGINE=TS

stmt=insert into stable values (1), (2), (3)

stmt=select count(*) from stable
[[3]]
stmt=select count(*) from stable
[[0]]
stmt=create  table test_sliding (a tinyint not null )TIMEBOUND('5s')

stmt=insert into test_sliding values (1), (2), (3)

stmt=select count(*) from test_sliding
[[3]]
stmt=select count(*) from test_sliding
[[0]]