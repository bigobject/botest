
stmt=find top 0.1 Product.name, Customer.gender, SUM from sales
invalid quantifier: top 0.1, a quantifier should be a positive integer or a percentage (1%~100%)
stmt=find 0.1 Product.name, Customer.gender, SUM from sales
invalid quantifier: 0.1, a quantifier should be a positive integer or a percentage (1%~100%)
stmt=find bottom 0.1 Product.name, Customer.gender, SUM from sales
invalid quantifier: bottom 0.1, a quantifier should be a positive integer or a percentage (1%~100%)
stmt=find total 200% Product.name, Customer.gender, SUM from sales
invalid quantifier: total 200%, a quantifier should be a positive integer or a percentage (1%~100%)
stmt=FIND summary('qqq') Product.name IN Customer.gender BY sum(Data) FROM sales
Lua error: /etc/bo/lua/summary.lua:4: unknown summary type : qqq
stmt=FIND `quantile`(25) Product.brand IN Customer.state BY sum(Data) FROM sales
Lua error: /etc/bo/lua/quantile.lua:3: quantile argument should be > 0 and <= 1
stmt=FIND `quantile`(-25) Product.brand IN Customer.state BY sum(Data) FROM sales
Lua error: /etc/bo/lua/quantile.lua:3: quantile argument should be > 0 and <= 1