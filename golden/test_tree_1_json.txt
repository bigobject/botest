
stmt=CREATE TREE testtree1 (v1 DOUBLE, SUM(Data), v2 INT32) FROM sales GROUP BY Product.brand, Customer.state

stmt=CREATE ALL TREE testtree3 (v1 DOUBLE, SUM(Data), v2 INT32) FROM sales GROUP BY Product.brand, Customer.state

stmt=CREATE TREE T1 () FROM Customer GROUP BY gender, name

stmt=CREATE ALL TREE T3 (SUM(Data), tmp_d DOUBLE) FROM sales GROUP BY Customer.gender, Product.brand

stmt=CREATE ALL TREE testtree4 (v1 DOUBLE, SUM(BAD_COL_NAME), v2 INT32) FROM sales GROUP BY Product.brand, Customer.state
["nknown column: 'BAD_COL_NAME"]
stmt=show trees
["T1", "T3", "testtree1", "testtree3"]