#!/bin/bash


echo "start python test"

rm -f ./python/test_output/*

#run python
for file in ./python/test_python/*/*; do
        if [[ -f $file ]] && [[ -s $file ]]; then
                dir=$(basename $(dirname $file))
		base=$(basename $file)
                if [[ "$1" = "kaiwu" ]] || [[ "$1" = "kaiwu2" ]];then
                        python3 $file kaiwu > ./python/test_output/$dir"_"${base%.*}"_kw".txt
                else
                        python3 $file > ./python/test_output/$dir"_"${base%.*}.txt
                fi	
        fi
done

#diff file
DIFF=$(diff -r ./python/test_output/ ./python/golden/ | grep -v '^Only in')
if [[ $DIFF == "" ]]; then
        echo "All test passed."
        echo "end python test"
else
        for file in ./python/test_output/*; do
                if [[ -f $file ]] && [[ -s $file ]]; then
                        base=$(basename $file)
                        DIFF=$(diff -q ./python/test_output/${base} ./python/golden/${base})
                        if [[ $DIFF != "" ]]; then
                                echo $DIFF
			fi
                fi
        done
        echo "end python test"
        exit 1
        
fi

