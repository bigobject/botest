import jaydebeapi
import sys


driver = "com.mysql.cj.jdbc.Driver"
jar_file = '/usr/share/java/mysql-connector-j-8.0.31.jar'
if len(sys.argv)==2  and sys.argv[1]=='kaiwu':
    jdbc_url = "jdbc:mysql://127.0.0.1:3306/kaiwudb?user=root"
    stmt="create table a(a int not null, b smallint not null,c bigint not null,d biginteger not null, e char not null,f string not null,g varstring not null,h float not null,i double not null,j date not null default '2022-01-01',k datetime not null default '2022-01-01 00:00:00',l datetime not null default '2022-01-01 00:00:00')"
else:
    jdbc_url = "jdbc:mysql://127.0.0.1:3306/bigobject"
    stmt="create table a(a int8 , b int16  ,c int32 ,d int64 , e char ,f string ,g varstring ,h float ,i double ,j date32 ,k datetime32 ,l datetime64 )"

cn = jaydebeapi.connect(jclassname=driver,url=jdbc_url,jars=jar_file)
cursor = cn.cursor()
cursor.execute(stmt)
cursor.execute("insert into a values(1,2,3,4,'abc','def','ghi',5.6,7.8,'2021-01-01','2022-02-02 10:11:12','2023-03-03 01:02:03')")
cursor.execute("select * from a")


result = cursor.fetchall()
print(result)

cursor.execute("drop table a")

cursor.close()
cn.close()
