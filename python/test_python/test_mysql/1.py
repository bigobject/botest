import mysql.connector
import sys
import getopt

try:
	#input command line
	my_host = '127.0.0.1'
	if len(sys.argv)==2  and sys.argv[1]=='kaiwu':
		my_user= 'root'
		my_password= ''
	else:
		my_user= None
		my_password= None
	table_name = None
	argv = sys.argv[2:]
	opts, args = getopt.getopt(argv, "u:h:d:p:t:")
	my_database = None 

	for opt, arg in opts:
		if opt in ['-h']:
			my_host = arg
		elif opt in ['-d']:
			my_database = arg
		elif opt in ['-u']:
			my_user = arg
		elif opt in ['-p']:
			my_password = arg
		elif opt in ['-t']:
			table_name = arg
			
	#connection
	
	if len(sys.argv)==2  and sys.argv[1]=='kaiwu':
		stmt="create table a(a int not null, b smallint not null,c bigint not null,d biginteger not null, e char not null,f string not null,g varstring not null,h float not null,i double not null,j date not null default '2022-01-01',k datetime not null default '2022-01-01 00:00:00',l datetime not null default '2022-01-01 00:00:00')"
	else:
		stmt="create table a(a int8 , b int16 ,c int32 ,d int64 , e char ,f string ,g varstring ,h float ,i double,j date32 ,k datetime32 ,l datetime64 )"
	cnx = mysql.connector.connect(user='%s'%my_user, host='%s'%my_host, password='%s'%my_password)
	cursor = cnx.cursor(buffered=True)
	cursor.execute(stmt)
	cursor.execute("set float_precision = 2")
	cursor.execute("set double_precision = 2")
	cursor.execute("insert into a values(1,2,3,4,'abc','def','ghi',5.6,7.8,'2021-01-01','2022-02-02 10:11:12','2023-03-03 01:02:03')")
	cursor.execute("select * from a")

	result = cursor.fetchall()
	print(result)

	cursor.execute("drop table a")
	
	if (cnx.is_connected()):
		cursor.close()
		cnx.close()

except mysql.connector.Error as err:
  	print("Something went wrong: {}".format(err))

	
