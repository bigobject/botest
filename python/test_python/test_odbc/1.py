import pyodbc 
import sys
if len(sys.argv)==2 and sys.argv[1]=='kaiwu':
    stmt="create table a(a int not null, b smallint not null,c bigint not null,d biginteger not null, e char not null,f string not null,g varstring not null,h float not null,i double not null, j date not null default '2022-01-01',k datetime not null default '2022-01-01 00:00:00',l datetime not null default '2022-01-01 00:00:00')"
    cn = pyodbc.connect("DRIVER={MySQL ODBC 8.0 ANSI Driver}; SERVER=127.0.0.1; PORT=3306; UID=root") 
else:
    stmt="create table a(a int8 , b int16 ,c int32 ,d int64 , e char ,f string ,g varstring ,h float ,i double ,j date32 ,k datetime32 ,l datetime64 )"
    cn = pyodbc.connect("DRIVER={MySQL ODBC 8.0 ANSI Driver}; SERVER=127.0.0.1; PORT=3306;") 

    

cursor = cn.cursor()
cursor.execute(stmt)
cursor.execute("set float_precision = 2")
cursor.execute("set double_precision = 2")
cursor.execute("insert into a values(1,2,3,4,'abc','def','ghi',5.6,7.8,'2021-01-01','2022-02-02 10:11:12','2023-03-03 01:02:03')")
cursor.execute("select * from a")

result = cursor.fetchall()
print(result)

cursor.execute("drop table a")

cursor.close()
cn.close()
