import re
import os


def codegen(logfile,pytestfile):
    log = open(logfile, 'r')
    lines = log.readlines()
    pyfile = open(pytestfile, "w")

    regex = re.compile(r"test:test_json/(.*)/(.*?)\.json (.*) (PASS|FAIL)")
    inforegex = re.compile(r"Please diff (.+?) (.+)")
    c = 0
    cate = ""
    for line in lines:
        if line == "BOTestMain\n":
            if cate == "":
                cate = "9090"
            else:
                cate = "3306"
        match = regex.search(line)
        if match:
            name = match.group(1) + "_" + match.group(2) + "_By" + cate
            status = match.group(4)
            if status == "FAIL":
                info = lines[c+1].replace("\n","")
                i = inforegex.search(info) 
                outputfile = i.group(1)
                goldenfile = i.group(2)
                info = "[output]\n"
                with open(outputfile) as ff:
                    outputdata = ff.readlines()
                    info = info + "\n".join(outputdata)
                info = info + "\n[golden]\n"
                with open(goldenfile) as ff:
                    goldendata = ff.readlines()
                    info = info + "\n".join(goldendata)
                sign = "!="
            else:
                info = ""
                sign = "=="
            func = "\n\ndef "+name+"():\n\tprint('''"+info.replace("'","\\'")+"''')\n\tassert 1 "+sign+" 1"
            pyfile.write(func)
        c = c + 1

    log.close()
    pyfile.close()

def execpytest(file,htmlfile):
    os.system('python3 -m pytest '+ file +' --html='+htmlfile+' --self-contained-html --no-summary')

def replacehtml(htmlfile):
    fin = open(htmlfile, "rt")
    data = fin.read()
    res_str = re.sub(r"def .*------------------------------Captured stdout call------------------------------ ", "", data)
    fin.close()
    fin = open(htmlfile, "wt")
    fin.write(res_str)
    fin.close()

def rmfile(file):
    os.system('rm '+file)


if __name__ == '__main__':
    codegen('raketest.log',"test_bo.py")
    execpytest("test_bo.py","report.html")
    replacehtml("report.html")
    rmfile("test_bo.py")