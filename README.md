# BOTEST #

Ruby tests for BigObject using rest api. 

## Input json file ##

Input for the test are in json format. Located in test_json/test_<category> directory. An example of the a json file:

<test_json/test_trim/1.json>

```
#!json

{
    "desc": "trim Customer table properly",
    "skip_platform":"ANDROID,INTEL",
    "setup": {
        "jsons": [
            "table.json"
        ]
    },
    "stmts": [
        "trim Customer 1",
        "select * from Customer",
        "trim Customer to 3",
        "select * from Customer",
        "trim Customer to 0",
        "select * from Customer"
    ],
    "teardown": {
        "jsons": [
            "table.json"
        ]
    }
}
```


**<setup>** - create table/associations and load csv files. In this example table.json (located in test_json/setup> contains the Customer/Product/sales data. Runs before <stmts>.

**<stmts>** - Commands to be tested, output redirected to <test_output/test_trim_1_json.txt>

**<teardown>** - drop table/associations here. Runs after <stmts>.

**<skip_platform>**- Optional, test will not execute when run in designated platform.

A golden output file for each test is stored in <golden> directory. Upon completion of each test, a "diff" is perform on test_output and the corresponding file in the golden folder. 

## How to run ##

Before running the test, please install the following gems :


```
#!bash

sudo gem install minitest-reporters
sudo gem install shoulda-context
sudo gem install curb
sudo gem install rest-client
sudo gem install json

```
### To run tests by script: ###
Optional type:
- default: run the tests with restapi by the default INTEL platform.
- mysql: run the tests with mysql by the default INTEL plaform.
- all: run the tests with both restapi and mysql by the default INTEL platform. (run this type if `<type>` is not assigned)
- kaiwu: run the tests with both restapi and mysql by KAIWU platform.
- kaiwu2: run the tests with both restapi and mysql by KAIWU2 platform.

```
sh botest.sh 
sh botest.sh <type>
```
The script will clear all the existing test_output and log files, and run the tests with the assigned type.


### To run the entire suite: ###
```
#!bash

rake
rake <type>

```
The command will run all test in - test_json.

### To run the suite with specific seed (order): ###
```
#!bash

SEED=16413 rake
SEED=16413 rake <type>
```

### To run tests in a sub-category: ###
```
#!bash

rake onetest[test_trim]
```
The command will run all test in test_json/test_trim/*.json. 

onetest is only available for INTEL platform.

### To run a single test: ###
```
#!bash

rake onetest[test_trim,1]
```
The command will run a single test test_json/test_trim/1.json

onetest is only available for INTEL platform.

### To run PLATFORM test: ###
Available input PLATFORM:
- INTEL
- ANDROID
- KAIWU
- KAIWU2
```
#!bash

rake test[<PLATFORM>]
rake test_mysql[<PLATFORM>]

```
The command will run most tests for specific PLATFORM. Some tests are skip intentionally as some features in BO are not supported in other platforms. Specify platform env and info in rakefile.

### To run a single `<PLATFORM>` test: ###
```
#!bash

rake one[<PLATFORM>,test_trim,1]
rake one_mysql[<PLATFORM>,test_trim,1]
```
The command will run a single test test_json/test_trim/1.json