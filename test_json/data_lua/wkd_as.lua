function wkd_as(d)
  wd = d:weekday()
  if (wd >= 1 and wd <= 5) then
    return 0
  else 
    return 1
  end
end
