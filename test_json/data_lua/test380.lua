function test380(a, dest_table)
    -- local dest = bo.getTable(dest_table)
    -- local code_tmp = bo.getTable(tmp_table)

    local dest = bo.getTable('tmp')
    -- local code_tmp = bo.getTable('code_tmp')

    -- local member_columns = {'CaseNo', 'Birthday', 'BatchNo', 'Name', 'Gender'}
    local member_columns = {'ID', 'CASE_NO', 'BirthDay', 'TARGET_DATE', 'BatchNo', 'Name', 'Gender', 'Contact_Id','B_TP', 'B_TN', 'B_FP', 'B_FN', 'B_EP', 'B_EN', 'B_MP', 'B_MN', 'B_WP', 'B_WN', 'D_TP', 'D_TN', 'D_FP', 'D_FN', 'D_EP', 'D_EN', 'D_MP', 'D_MN', 'D_WP', 'D_WN', 'S_E1', 'S_E2', 'S_E3', 'S_E4', 'S_E5', 'S_E6', 'S_E7', 'S_E8', 'S_E9', 'S_E0', 'S_E', 'M_E1', 'M_E2', 'M_E3', 'M_E4', 'M_E5', 'M_E6', 'M_E7', 'M_E8', 'M_E9', 'M_E0', 'M_E'}

    local code_columns = {'TARGET_DATE', 'TP', 'TN', 'FP', 'FN', 'EP', 'EN', 'MP', 'MN', 'WP', 'WN'}

    id = tonumber(a:id())

    -- code_tmp_table = {}
    -- code_tmp_size = code_tmp:size()

    -- print(id, dest, code_tmp, dest:size(), code_tmp_size, #member_columns)
    -- print(id, dest_table, dest)
    -- print('seg1')

    -- for i=1, code_tmp_size do
    --     local code_row = code_tmp:getValue(i, code_columns)
    --     -- code_tmp_table[i] = {code_row[1], tonumber(code_row[2]), tonumber(code_row[3]), tonumber(code_row[4]), tonumber(code_row[5]), tonumber(code_row[6]), tonumber(code_row[7]), tonumber(code_row[8]), tonumber(code_row[9]), tonumber(code_row[10]), tonumber(code_row[11])}
    --     code_tmp_table[i] = code_tmp:getValue(i, code_columns)
    -- end

    -- print('seg2')
    -- print(type(id))

    local row = dest:getValue(
        id,
        member_columns
        )

    -- local member_columns = {'ID', 'CASE_NO', 'BirthDay', 'TARGET_DATE', 'BatchNo', 'Name', 'Gender', 'Contact_Id','B_TP', 'B_TN', 'B_FP', 'B_FN', 'B_EP', 'B_EN', 'B_MP', 'B_MN', 'B_WP', 'B_WN', 'D_TP', 'D_TN', 'D_FP', 'D_FN', 'D_EP', 'D_EN', 'D_MP', 'D_MN', 'D_WP', 'D_WN', 'S_E1', 'S_E2', 'S_E3', 'S_E4', 'S_E5', 'S_E6', 'S_E7', 'S_E8', 'S_E9', 'S_E0', 'S_E', 'M_E1', 'M_E2', 'M_E3', 'M_E4', 'M_E5', 'M_E6', 'M_E7', 'M_E8', 'M_E9', 'M_E0', 'M_E'}
    local CaseNo, Birthday, BatchNo, Name, Gender = row[2], row[3], row[5], row[6], row[7]
    local Birthday, B_TP, B_TN, B_FP, B_FN, B_EP, B_EN, B_MP, B_MN, B_WP, B_WN = row[3], row[9], row[10], row[11], row[12], row[13], row[14], row[15], row[16], row[17], row[18]
    local TARGET_DATE, D_TP, D_TN, D_FP, D_FN, D_EP, D_EN, D_MP, D_MN, D_WP, D_WN = row[4], row[19], row[20], row[21], row[22], row[23], row[24], row[25], row[26], row[27], row[28]


    -- print('3')
    -- print('4', Gender)

    -- Weight calculation
    S_E1=0.5*(B_TP+D_TP*(1-((B_WN+D_WN)*0.5+(B_FN+D_FN)*0.25-(B_MP+D_MP)*0.5-(B_EP+D_EP)*0.25)))
    S_E2=0.5*(B_TN+D_TN*(1-((B_WP+D_WP)*0.5+(B_EP+D_EP)*0.25-(B_MN+D_MN)*0.5-(B_FN+D_FN)*0.25)))
    S_E3=0.5*(B_FP+D_FP*(1-((B_TN+D_TN)*0.5+(B_MN+D_MN)*0.25-(B_WP+D_WP)*0.5-(B_EP+D_EP)*0.25)))
    S_E4=0.5*(B_FN+D_FN*(1-((B_TP+D_TP)*0.5+(B_MP+D_MP)*0.25-(B_WN+D_WN)*0.5-(B_EN+D_EN)*0.25)))
    S_E5=0.5*(B_EP+D_EP*(1-((B_FN+D_FN)*0.5+(B_WN+D_WN)*0.25-(B_TP+D_TP)*0.5-(B_MP+D_MP)*0.25)))
    S_E6=0.5*(B_EN+D_EN*(1-((B_FP+D_FP)*0.5+(B_WP+D_WP)*0.25-(B_TN+D_TN)*0.5-(B_MN+D_MN)*0.25)))
    S_E7=0.5*(B_MP+D_MP*(1-((B_EN+D_EN)*0.5+(B_TN+D_TN)*0.25-(B_FP+D_FP)*0.5-(B_WP+D_WP)*0.25)))
    S_E8=0.5*(B_MN+D_MN*(1-((B_EP+D_EP)*0.5+(B_TP+D_TP)*0.25-(B_FN+D_FN)*0.5-(B_WN+D_WN)*0.25)))
    S_E9=0.5*(B_WP+D_WP*(1-((B_MN+D_MN)*0.5+(B_FN+D_FN)*0.25-(B_EP+D_EP)*0.5-(B_TP+D_TP)*0.25)))
    S_E0=0.5*(B_WN+D_WN*(1-((B_MP+D_MP)*0.5+(B_FP+D_FP)*0.25-(B_EN+D_EN)*0.5-(B_TN+D_TN)*0.25)))
    -- print('4-1')

    if Gender == 1 then
        S_E1=S_E1*1.02
        S_E3=S_E3*1.02
        S_E5=S_E5*1.02
        S_E7=S_E7*1.02
        S_E9=S_E9*1.02
    end

    if Gender == 2 then
        S_E2=S_E2*1.02
        S_E4=S_E4*1.02
        S_E6=S_E6*1.02
        S_E8=S_E8*1.02
        S_E0=S_E0*1.02
    end

    if Gender == 0 then
        S_E1=S_E1*1.01
        S_E3=S_E3*1.01
        S_E5=S_E5*1.01
        S_E7=S_E7*1.01
        S_E9=S_E9*1.01
        S_E2=S_E2*1.01
        S_E4=S_E4*1.01
        S_E6=S_E6*1.01
        S_E8=S_E8*1.01
        S_E0=S_E0*1.01
    end
    S_E=S_E1+S_E2+S_E3+S_E4+S_E5+S_E6+S_E7+S_E8+S_E9+S_E0

    M_E1=S_E1/S_E
    M_E2=S_E2/S_E
    M_E3=S_E3/S_E
    M_E4=S_E4/S_E
    M_E5=S_E5/S_E
    M_E6=S_E6/S_E
    M_E7=S_E7/S_E
    M_E8=S_E8/S_E
    M_E9=S_E9/S_E
    M_E0=S_E0/S_E

    M_E=M_E1+M_E2+M_E3+M_E4+M_E5+M_E6+M_E7+M_E8+M_E9+M_E0
    -- print('5')

    -- dest:insert({
    --     id,
    --     CaseNo,
    --     Birthday,
    --     TARGET_DATE,
    --     BatchNo,
    --     Name,
    --     Gender,
    --     '',
    --     B_TP, B_TN, B_FP, B_FN, B_EP, B_EN, B_MP, B_MN, B_WP, B_WN,
    --     D_TP, D_TN, D_FP, D_FN, D_EP, D_EN, D_MP, D_MN, D_WP, D_WN,
    --     S_E1, S_E2, S_E3, S_E4, S_E5, S_E6, S_E7, S_E8, S_E9, S_E0, S_E,
    --     M_E1, M_E2, M_E3, M_E4, M_E5, M_E6, M_E7, M_E8, M_E9, M_E0, M_E
    --     })
    dest:setValue(
        id,
        {['S_E1']=S_E1, ['S_E2']=S_E2, ['S_E3']=S_E3, ['S_E4']=S_E4, ['S_E5']=S_E5, ['S_E6']=S_E6, ['S_E7']=S_E7, ['S_E8']=S_E8, ['S_E9']=S_E9, ['S_E0']=S_E0, ['S_E']=S_E, ['M_E1']=M_E1, ['M_E2']=M_E2, ['M_E3']=M_E3, ['M_E4']=M_E4, ['M_E5']=M_E5, ['M_E6']=M_E6, ['M_E7']=M_E7, ['M_E8']=M_E8, ['M_E9']=M_E9, ['M_E0']=M_E0, ['M_E']=M_E}
        )
    -- print('6')

    return true
end
