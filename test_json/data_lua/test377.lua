function test377(bt, col, rt_col)
   t = bo.getTable(bt)
   rt = t:getColumnValue(col);
   for i = 2, #rt do
      rt[i] = rt[i] + rt[i-1]
   end
   t:setColumnValue(rt_col, rt)
end
