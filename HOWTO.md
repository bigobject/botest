# HOW TO ADD NEW BOTEST #

This document introduce the process of adding new tests or platforms. 

## Add New Platform ##
The following is an example of adding KAIWU platform.

### Default settings ###
Add platform default settings in rakefile. 
```
task :setenv_kaiwu do
  ENV['BIGOBJECT_RESTURL']='http://127.0.0.1:9090/'
  ENV['BIGOBJECT_SERVER']='127.0.0.1'
  ENV['BOTEST_PLATFORM']='KAIWU'
  ENV['BOTEST_SKIP']=''
 end
```

### Task ###
Add task in rakefile. Assigned env settings first and the following tests. 
```
task :kaiwu => [:setenv_kaiwu, :test, :test_mysql]
```

### Settings in env ###
Add env setting for input platform in rakefile.  
```
task :env, [:platform] do |t, args|
  puts "Platform: #{args}" 
  if args[:platform]=='ANDROID'
     Rake::Task["setenv_android"].invoke
  elsif args[:platform]=='KAIWU'
    Rake::Task["setenv_kaiwu"].invoke
  else
     Rake::Task["setenv"].invoke
  end
end 
```


## Add New Tests ##
1. Add tests in test_json/test_ directory.
2. Add golden output files (`<test_name>`_`<no>`_json.txt) in both golden (restapi) and test_mysql/golden (mysql) directories.