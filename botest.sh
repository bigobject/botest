#!/bin/bash


echo "start botest"

logfile="raketest.log"
rm -fr $logfile
rm -fr test_output/test*
rm -fr test_mysql/test_output/test*
if [ -z "$1" ] && [ "$1" != "kaiwu" ] && [ "$1" != "kaiwu2" ]
then
    rake all > $logfile
else
    if [ "$1" = "kaiwu" ]
    then
        rake one["KAIWU"] >> $logfile
        rake one_mysql["KAIWU"] >> $logfile
    elif [ "$1" = "kaiwu2" ]
    then
      rake one["KAIWU2"] >> $logfile
      rake one_mysql["KAIWU2"] >> $logfile
    else
        rake $1 > $logfile
    fi  
fi
if [ -f $logfile ] && [ -s $logfile ]; then
  FAILCOUNT=`grep FAIL raketest.log | wc -l | cut -f1 -d' '`
  ERRORCOUNT=`grep ERROR raketest.log | wc -l | cut -f1 -d' '`

   if [ $FAILCOUNT -eq 0 ] &&  [ $ERRORCOUNT -eq 0 ]; then
     echo "All test passed."
     python3 genreport.py || true
   else
     echo "Error in botest. Check $logfile in <workspace>."
     python3 genreport.py || true
     exit 1
   fi
else 
   echo "Error in botest. Tests not run. Check console output."
   exit 1
fi 

echo "end botest"
