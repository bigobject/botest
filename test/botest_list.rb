class BOTestList

  @@testdir = "test_json"
  
  def initialize(onetestdir, onetest)
    @onetestdir = onetestdir
    @onetest = onetest
  end
  
  def allTests()
    return (@onetestdir==nil) ? "#{@@testdir}/test_*/*.json" :
                                ((@onetest==nil)? "#{@@testdir}/#{@onetestdir}/*.json": 
                               "#{@@testdir}/#{@onetestdir}/#{@onetest}.json")
  end
  
  def findTests()
    skip = ENV['BOTEST_SKIP']
    puts "BOTEST_SKIP:#{skip}"
    skip_tests = (skip.to_s =='') ? Array.new : skip.split(',')
    
    tests = allTests()        
    alltests=Dir.glob(tests)
    finalTests = Array.new
    
    #filter out skip_tests
    if !(skip_tests.empty?)
        alltests.each do |test|
          doskip = false
          skip_tests.each do |skip_test|
            skip_test="#{@@testdir}/#{skip_test.strip()}"
            if (test.start_with?(skip_test))
              doskip = true
              break;
            end
          end
          finalTests.push(test) if !doskip
      end
    end
    
    return (finalTests.empty?)? alltests: finalTests
  end
end