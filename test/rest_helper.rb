require 'net/http'
require 'rest-client'

class RestHelper
  
  def initialize(testurl=nil)
    testurl=(testurl)? testurl:ENV['BIGOBJECT_RESTURL']
    @cmdurl = testurl +'api'
    @scripturl = testurl +'script/'  
    @resource = RestClient::Resource.new @cmdurl, :timeout => 3600, :open_timeout => 3600
  end
  
  def formatRequest(stmt, workspace, params)
    hdata = Hash.new
    hdata['stmt'] = stmt
    hdata['workspace'] = workspace if workspace.to_s!=''
    hdata['params'] = params if (params != nil && params.length > 0) 
    return hdata
  end
  
  def postCmd(stmt, workspace="", params=nil)
    begin
      hdata = formatRequest(stmt, workspace, params)
#      puts "\ncurl --request POST #{@cmdurl} --data '#{hdata.to_json}'"
      resp = @resource.post hdata.to_json, :content_type =>:json, :accept =>:json
                                          
#      puts "resp= #{resp.body}"
    rescue Exception => e 
      puts e.message  
      raise "Failed to postCmd: #{e.message}"
    end
    return resp
  end
  
  def postCmds(stmts, workspace="", params=nil)
    begin
      stmts.each do |stmt|
        hdata = formatRequest(stmt, workspace, params)   
        #puts "\ncurl --request POST #{@cmdurl} --data '#{params.to_json}'"
        resp = @resource.post hdata.to_json, :content_type =>:json, :accept =>:json
        #puts "resp= #{resp.body}"
      end
    rescue Exception => e 
      puts e.message  
      raise "Failed to postCmds: #{e.message}"
    end      
    return resp
  end
  
  def postScript(pathToFile)
    begin
      resp = RestClient.post @scripturl, :name_of_file_param => File.new(pathToFile)
     rescue Exception => e 
       puts e.message  
       raise "Failed to postScript: #{e.message}"
    end      
  end
  
  
end
