
class BOTestMysql < BOTestBase
  
  def initialize(dir, base, stmts,user_login)
		doUser = (user_login)? true:false
    super(File.basename(dir) + "_" + base.gsub('.', '_'))
    @stmts = stmts
		if !doUser
			@bo = ENV['BIGOBJECT_SERVER']
		end
		if doUser
			@bo = user_login
		end
  end
  
  def getMysqlFileName(index)
    "./test_mysql/sql/#{@methodName}_#{index}.sql"
  end
  
  def getTestOutputFileName()
    "./test_mysql/test_output/#{@methodName}.txt"
  end
  
  def getGoldenFileName()
    "./test_mysql/golden/#{@methodName}.txt"
  end 
  
  
  def specialStmt(stmt)
    stmt.match(/^sh:/)? true:false
  end  

  def runAndOutput()
    begin
      index = 1
      cnt = 0
      fullname = getMysqlFileName(index)
      aFile = File.new(File.expand_path(fullname), "w")
      oFilename = getTestOutputFileName()
      File.delete(oFilename) if File.exist?(oFilename)
      
      @stmts.each do |stmt|
        if (specialStmt(stmt))
          if (cnt>0)
            aFile.close()
            value = `mysql --force -h #{@bo}  < #{fullname} >> #{oFilename} 2>&1`
            puts "mysql: #{value}"
            cnt = 0;
          end 
          cmd=stmt[3..-1].lstrip
          puts "Running shell cmd:#{cmd}"
          `#{cmd}` 
          index += 1
          fullname = getMysqlFileName(index)
          aFile = File.new(File.expand_path(fullname), "w")
          next
        else
          writeOutput(aFile, "#{stmt};\n")
          cnt += 1
        end
      end
      if (cnt>0)
        aFile.close()
        value = `mysql --force -h #{@bo}  < #{fullname} >> #{oFilename} 2>&1`
#        puts "mysql: #{value}"
      end
    ensure
      aFile.close() if (aFile && !aFile.closed?)
    end
  end
  
end
