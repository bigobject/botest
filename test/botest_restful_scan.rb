require 'json'
require_relative 'rest_helper'

class BOTestRestfulScan < BOTestBase

def initialize(stmts, methodName, startIndex, endIndex, workspace='')
    super(methodName)
    @stmts = stmts
    @startIndex = startIndex
    @endIndex = endIndex
    @workspace = workspace #not used
  end
  
  def runAndOutput()
    fname = getTestOutputFileName()
    aFile = File.new(File.expand_path(fname), "w")
    restHelper = RestHelper.new
    gclist = Array.new
    
    @stmts.each do |stmt|
      next if (specialStmt(stmt))
        
      doScan = false
      params = nil
      #only find/get/select can be used with handle
      if (stmt.downcase.match(/^(find|get|select)/))
        params = Hash.new
        params['handle'] = true
        doScan = true
      end
      
      resp = restHelper.postCmd(stmt, @workspace, params)
      ret = JSON.parse(resp)
      if (!doScan)
          writeOutput(aFile, "\nstmt=#{stmt}\n")
          next
      end
  
      writeOutput(aFile, "\nstmt=#{stmt} with SCAN\n")    
      scanID = ret["results"]["res"]
      gclist.push(scanID)
  
      # recursively scan
      index = @startIndex
      while (index >=0)
        scanStmt = "scan #{scanID}"
        scanStmt = "#{scanStmt} #{index}" if (index>0)
        scanStmt = "#{scanStmt} #{@endIndex}" if (@endIndex > 0)
        resp = restHelper.postCmd(scanStmt, @workspace)
        ret = JSON.parse(resp)
        content = (ret["retcode"]==0)? ret["results"] : ret["errmsg"]
        writeOutput(aFile, "#{content.to_s}\n")
        index = ret['meta']['next']
        break if (index>@endIndex)
      end
      
    end
    ensure
      aFile.close() if aFile
      gclist.each do |scanid|
        resp = restHelper.postCmd("gc del #{scanid}", @workspace)
      end if gclist
  end
  
 end
  
