require 'json'
require_relative 'rest_helper'

class BOTestBase
  
  def initialize(methodName='')
    @methodName = methodName
  end
  
  def getGoldenFileName()
    "./golden/#{@methodName}"
  end
  
  def getTestOutputFileName()
    "./test_output/#{@methodName}"
  end
  
  def writeOutput(aFile, str)
    ret = aFile ? (aFile.syswrite(str)) : (print str)
  end
  
  def compareFile()
    file1 = getGoldenFileName()
    file2 = getTestOutputFileName()
    
    return FileUtils.compare_file(file1, file2), "Please diff #{file2} #{file1}"
  end
  
  def specialStmt(stmt)
    if (stmt.match(/^#/))
      return true
    elsif (stmt.match(/^sh:/))
      # shell command
      cmd=stmt[3..-1].lstrip
      puts "Running shell cmd:#{cmd}"
      `#{cmd}`
      return true
    end
    return false
  end
  
  # use <workspace>
  def workspaceStmt(stmt)
    return (stmt.match(/^use/))? stmt[3..-1].lstrip: nil
  end
  
  def getWorkspace(stmt, curWorkspace)
    if (stmt.match(/^create workspace/))
      workspace = nil
    else
      ws = workspaceStmt(stmt)
      workspace = (ws != nil)? ws : curWorkspace
    end
    
    return workspace
  end
  
end