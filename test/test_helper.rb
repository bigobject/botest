require 'minitest/autorun'
require 'minitest/reporters' # requires the gem
require 'shoulda/context'
 
Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new # spec-like progress

class Minitest::Test
  @@datasource= ENV['TESTBO_DATASOURCE']   
  @@addr_info = ENV['BIGOBJECT_RESTURL']
  @@websocket_url = ENV['BIGOBJECT_WEBSOCKETURL']
end


