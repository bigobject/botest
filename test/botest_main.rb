require 'json'
require "minitest/autorun"
require 'csv'

require_relative 'botest_base'
require_relative 'test_helper'
require_relative 'botest_smartloader'
require_relative 'botest_mysql'
require_relative 'botest_list'
require_relative 'botest_restful'
require_relative 'botest_restful_scan'

class BOTestMain < Minitest::Test
 
   @@botcp = "/dev/tcp/#{ENV['BIGOBJECT_SERVER']}/9091"
   @@bo_platform = ENV['BOTEST_PLATFORM']
  
  def loadTableFromCSV(tableName, csvFile)
     insertStmts = Array.new
     CSV.foreach(csvFile) do |row|
       stmt = "(\"#{row.join("\",\"")}\")"
       insertStmts.push(stmt);
     end
     stmt = "insert into #{tableName} values#{insertStmts.join(',')}"
     restHelper = RestHelper.new
     restHelper.postCmd(stmt)
  end
  
  def doStmtsAndLoadCsvs(testJson)
    stmts = testJson["stmts"]
    if (stmts!=nil)
      test = ::BOTestRestful.new(stmts)
      test.boExecute()
    end
    
    loadcsvs = testJson["loadcsv"]
    if (loadcsvs != nil)
      loadcsvs.each do |table, csvfile|
        csv = "./test_json/data_csvs/#{csvfile}"
        loadTableFromCSV(table, csv)
      end
    end
  end
  
  def doJsons(jsons, dir)
    jsons.each do |oneJson|
      myJson = "#{dir}/#{oneJson}"
      testJson = JSON.parse(File.read(myJson));
      doStmtsAndLoadCsvs(testJson)
    end
  end
  
  def doSmartLoadCsvs(smartloadcsv)
    dir = smartloadcsv["dir"]
    csvs = smartloadcsv["csvs"]
    smartloader = SmartLoader.new
    csvs.each do |table|
      smartloader.uploadCSV(dir, table)
    end  
  end
  
  def to_boolean(str)
     str!=nil && str.downcase == 'true'
  end
  
  def failedMsg(fname)
    return "Please diff #{getTestOutputFileName(fname)} #{getGoldenFileName(fname)}"
  end
  
  def uploadLuaScript(lua)
    helper = RestHelper.new
    helper.postScript(lua)
  end
  
  # handle startup/teardown
  def doPrePost(testJsons, jsonDir)
    if (testJsons!=nil)
      json = testJsons["jsons"]
      doJsons(json, jsonDir) if (json!=nil)
      
      smartloadcsv = testJsons["smartloadcsv"]
      doSmartLoadCsvs(smartloadcsv) if (smartloadcsv!=nil)
      
      doStmtsAndLoadCsvs(testJsons) 
      
      uploadlua = testJsons["uploadlua"]
      if (uploadlua != nil)
        uploadlua.each do |luafile|
          uploadLuaScript("./test_json/data_lua/#{luafile}")
        end
      end
    end
  end
  
  
  
    # main 
    domysql = (ARGV[0]=='mysql')? true:false
    argbase = (domysql)? 1:0
        
    lister = BOTestList.new(ARGV[argbase], ARGV[argbase+1])
    jsons = lister.findTests()
    
    jsons.each do |json_file|
      context "#{json_file}" do
         
        testJson = JSON.parse(File.read(json_file));
        desc = testJson["desc"]
        skip_platform = testJson["skip_platform"]
	user_login = testJson["user_login"]

        doSkip = (skip_platform.to_s.split(",").include? @@bo_platform)? true:false
          
        setup do
           if !doSkip
             setupJsons = testJson["setup"]
             doPrePost(setupJsons, "./test_json/setup")
           end
        end
         
        teardown do
          if !doSkip
             teardownJsons = testJson["teardown"]
             doPrePost(teardownJsons, "./test_json/teardown")
          end
        end
        
        should "#{desc}" do
          
          if doSkip
             puts "skip #{skip_platform}"
             skip "#{skip_platform}"
             next
          end
                  
          stmts = testJson["stmts"]
          next if stmts==nil
          
          useScan = testJson["use_scan"]
                      
          dir, base = File.split(json_file)
          fname = File.basename(dir) + "_" + base.gsub('.', '_') + ".txt"
           
          if (domysql)
            test = BOTestMysql.new(dir,base,stmts,user_login)
          else
            test = (useScan)?
              BOTestRestfulScan.new(stmts, fname, (useScan["start"]=='')?0:useScan["start"], (useScan["end"]=='')?0:useScan["end"]) :
              BOTestRestful.new(stmts, fname, to_boolean(testJson["sortByFirstKey"]), testJson["params"])            
          end
          
          test.runAndOutput()
          isSame, fMsg = test.compareFile()
          assert isSame, fMsg
        end
      end
    end

end

