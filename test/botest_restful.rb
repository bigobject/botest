require 'json'
require_relative 'rest_helper'

class BOTestRestful < BOTestBase

  def initialize(stmts, methodName="", sortByFirstKey=false, jparams = nil)
    super(methodName)
    @stmts = stmts
    @printStmts = true
    @sortByFirstKey = sortByFirstKey
    @jparams = jparams
  end
  
  def boExecute()
    restHelper = RestHelper.new
    @stmts.each do |stmt|
      next if (specialStmt(stmt))
      resp = restHelper.postCmd(stmt)
    end
  end
  
  # str : [["Pepsi", 37.7], ["Minute Maid", 33.04], ["Cheetos", 79]]
  # str : [["Pepsi", "Minute Maid", "Matthew Harper"], ["Pepsi", "Cheetos", "Matthew Harper"], ["Pepsi", "Cheetos", "Walter Parker"], ["Pepsi", "Ocean Spray", "Matthew Harper"], ["Pepsi", "Aquarius", "Matthew Harper"]]
  def insertToArray(str)
    str = str[1..-2] #remove bracket
    strArray = str.split('], ')
    array = Array.new
    strArray.each do |item|
      item=item.delete('[]"')
      array.push(item)
    end
    return array
  end

  def runAndOutput()
      fname = getTestOutputFileName()
      aFile = File.new(File.expand_path(fname), "w")
      restHelper = RestHelper.new
      params = Hash.new
      workspace = nil
      
      @stmts.each do |stmt|
        next if (specialStmt(stmt))
        workspace = getWorkspace(stmt, workspace)
        
        #only find/get/select can be used with handle
        if (@jparams != nil && stmt.downcase.match(/^(find|get|select)/))
          params["size"] = @jparams["size"]  
        end
        
        resp = restHelper.postCmd(stmt, workspace, params)
        
        ret = JSON.parse(resp)
        content = (ret["retcode"]==0)? ret["results"] : ret["errmsg"]
        str=(@printStmts)? "\nstmt=#{stmt}\n" : "\n"
        writeOutput(aFile, str)
        
        if (@sortByFirstKey && content.to_s!= '')
          array = insertToArray(content.to_s)
          writeOutput(aFile, array.sort)
        else
          writeOutput(aFile, content.to_s)
        end 
      end
      ensure
        aFile.close() if aFile
  end
  
 end
  
