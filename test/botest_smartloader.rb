require 'json'
require 'csv'
require 'curb'
require_relative 'test_helper'

class SmartLoader
  
  def cleanup(csvs)
    csvs.each do |table, csvfile|
      restHelper = RestHelper.new
      restHelper.postCmd("drop table #{table}", "")
    end
  end
  
  def upload(url, csvfile) 
    system(`curl -F 'file=@#{csvfile};type=text/csv' #{url}`)
  end

  def commit(url, schema)
    commitURL = url + "&action=create"
    `curl -X PUT -H 'Content-Type: application/json' -d '#{schema}' '#{commitURL}'`
  end
  
  def uploadCSV(dir, table)
    bourl = ENV['BIGOBJECT_RESTURL'].chomp("/")
    csvfile = dir + "/" + table + ".csv"
    
    begin
        #get callback_url
	impport_url = bourl+'/import'
        http = Curl.post(impport_url)
        ret = JSON.parse(http.body_str)
        callback_url = ret['callback_url']
        session = callback_url.sub("/import/","")
        
        #upload data
	url = bourl + callback_url
        upload(url, csvfile)
       
        #get schema
        http = Curl::Easy.perform(url)	

        #commit import data 
        commit(url, http.body_str.chomp("\n"))
         
	url = impport_url + "/status/" + session
	loop do
            http = Curl::Easy.perform(url)
            break if http.body_str == "done"
            sleep 2
     	end
	# wait for trailing data
	sleep 1
 
      rescue Exception => e 
        puts e.message  
#        puts e.backtrace.inspect  
        raise "Failed to post: #{e.message}"
      end    

  end
  
end
